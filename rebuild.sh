#!/bin/bash

# 第一步：查询并解除端口占用
echo "Checking and releasing port 25500..."
pid=$(lsof -i:25500 -t)
if [ -n "$pid" ]; then
    echo "Process using port 25500 found (PID: $pid). Killing the process..."
    kill -9 $pid
    echo "Process killed."
else
    echo "Port 25500 is not in use."
fi

# 第二步：执行make clean和make html
echo "Executing make clean..."
make clean

echo "Executing make html..."
make html

# 第三步：进入目录./build/html，启动简单的HTTP服务器
cd ./build/html
echo "Starting Python HTTP server on port 25500..."
python3 -m http.server 25500

echo "Script execution completed."
