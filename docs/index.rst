.. How To Use Sphinx documentation master file, created by
   sphinx-quickstart on Wed Jan 24 13:08:21 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

How To Use Sphinx
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   oprations/index


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
