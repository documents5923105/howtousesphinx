RstIntro
==============

打开source下的index.rst，也就是文档的起始界面。

.. image:: ./index.png
    :alt: index.rst
    :align: center

------------------------

这是一个采用rst标记语言书写的页面。

sphinx中采用的rst标记语言来生成网页，rst系统性的学习可以参考此 `网站 <https://wklchris.github.io/blog/reStructuredText/Intro.html>`_ 。

该网站本身采用rst语言写就，可以方便地查看rst源码，进行对比学习。
