Sphinx是什么
================


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   


Sphinx 是一个基于 Python 的文档生成项目。最早只是用来生成 Python 的项目文档，使用 reStructuredText 格式。

reStructuredText(rst)是一种轻量级标记语言。旨在建立一套标准文本结构化格式用以将文档转化为有用的数据格式（简单来说，就是要实现一套简单、直观、明确、原文本可阅读的，且可以转化为其他格式的文档标记语言）。

rst 与 Markdown 非常相似，都是轻量级标记语言。Markdown 的目标较rst更简单，就是为了简单地写 HTML，完成 text-to-HTML 的任务。


