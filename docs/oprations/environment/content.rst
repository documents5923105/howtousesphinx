Environment
====================

一、以 CentOS 系统为例（各种Linux发行版，MacOS和windows均可），首先安装python3、python3-pip和make。
-----------------------------------------------------------------------------------------------------------

``yum install -y make``

``yum install -y python3``

``yum install -y phthon3-pip``

--------------

二、安装最新版本Sphinx及依赖
------------------------------------

``pip3 install -U Sphinx``

完成后，系统会增加 

``sphinx-quickstart,  sphinx-build`` 

等以 ``sphinx`` 为开头的命令。






