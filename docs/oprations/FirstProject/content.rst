FirstProject
==============================

接下来借助 *sphinx-quickstart* 来生成一个项目练手：
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

一、创建项目
-------------------
在工作目录下（比如MySphinx）执行 *sphinx-quickstart* ,出现如下对话：

---------

*Welcome to the Sphinx 5.3.0 quickstart utility*

*Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).*

*Selected root path: .*

*You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.*

*> Separate source and build directories (y/n) [n]: y*

----------

sphinx-quickstart提供两种生成方式，一种是生成_build目录，一种是生成build和source目录分开保存。选择y是以第二种模式进行quickstart

然后按照提示输入项目名称，作者和版本信息，语言可以选择简体中文。

---------------

*The project name will occur in several places in the built documentation.*

*> Project name:  MySphinx*

*> Author name(s): alan*

*> Project release []: v0.1*

*If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.*

*For a list of supported codes, see
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.*

*> Project language [en]: zh_CN*

---------------

两种生成方式的目录结构如下

.. image:: ProjectStructure.png
    :alt: ProjectStructure
    :align: center

一些内容解释：

``build`` :生成文件的输出目录。

``Makefile``: 定义了多种make模式，以加target的方式选择，具体可使用 ``make`` 来查询。

``conf.py``: 配置文件。

``index.rst``: 文档起始文件。

到这里为止，已经完成了文档的创建工作。接下来，在MySphinx下执行命令 ``make html`` ，会在 ``./build/html`` 目录下生成html相关文件。

进入 ``./build/html`` ，执行命令 ``python3 -m http.server 25500``，就可以在浏览器通过IP：端口号来访问文档了。

初始浏览界面比较简朴，可以在conf.py中修改 ``html_theme`` 字段。最常用的主题是sphinx_rtd_theme，需要安装，安装指令为 ``pip3 install sphinx_rtd_theme``。呈现的效果就是大家最熟悉的蓝白主题界面，比如这种。

.. image:: theme.png
    :alt: theme
    :align: center