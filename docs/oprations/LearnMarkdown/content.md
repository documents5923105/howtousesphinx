# LearMarkDown

作为被广泛应用的文档书写语言，markdown并未被sphinx原生支持，需要下载markdown插件来使用。使用命令

`pip3 install recommonmark`

来安装markdown插件。

-----------
打开conf.py文件，向extensions中添加'recommonmark'

`extensions = ['recommonmark']`

关于对MarkDown的学习，可以参见[网站](https://markdown.com.cn/editor/#jump_8)。该网站优势在于对比学习，可以迅速在实践中熟悉MarkDown的语法。

MarkDown能够实现的功能不如rst丰富，但是如果有较好的超文本标记语言(html)基础，MarkDown可以内嵌html语法，来实现更加精细的功能。

-------------
本文档由markdown标记语言书写，可以点击右上角“查看页面源码”进行查看。如果不想允许浏览者查看页面源码，则需要在conf.py文件中添加 `html_show_sourcelink = False` 